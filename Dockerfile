# user phusion passenger image as it has a lot of stuff needed to run rails apps correctly
# see https://github.com/phusion/passenger-docker for documentation
FROM phusion/passenger-ruby27

ENV HOME /home/app

# install ffmpeg (instruction: https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu)
RUN apt-get update
RUN apt-get -y install autoconf automake build-essential libass-dev libfreetype6-dev libgpac-dev \
  libsdl1.2-dev libtheora-dev libtool libva-dev libvdpau-dev libvorbis-dev libx11-dev \
  libxext-dev libxfixes-dev pkg-config texi2html zlib1g-dev libx264-dev 
RUN apt-get install -y libtiff5-dev libjpeg8-dev libopenjp2-7-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python3-tk libharfbuzz-dev libfribidi-dev libxcb1-dev
RUN mkdir ~/ffmpeg_sources
RUN apt-get install -y yasm wget libmp3lame-dev libopus-dev

# libfdk-aac
RUN cd ~/ffmpeg_sources \
 && git clone https://github.com/mstorsjo/fdk-aac \
 && cd fdk-aac \
 && autoreconf -fiv \
 && ./configure --prefix="$HOME/ffmpeg_build" --disable-shared \
 && make \
 && make install \
 && make distclean

# libvpx
RUN cd ~/ffmpeg_sources \
 && wget https://chromium.googlesource.com/webm/libvpx/+archive/148d1085f79c6b4dd07d552cb51b53bd2e87a3aa.tar.gz \
 && mkdir libvpx \
 && tar xvf 148d1085f79c6b4dd07d552cb51b53bd2e87a3aa.tar.gz -C libvpx \
 && cd libvpx \
 && PATH="$HOME/bin:$PATH" ./configure --prefix="$HOME/ffmpeg_build" --disable-examples \
 && PATH="$HOME/bin:$PATH" make \
 && make install \
 && make clean

# now ffmpeg itself
RUN cd ~/ffmpeg_sources \
 && wget http://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2 \
 && tar xjvf ffmpeg-snapshot.tar.bz2 \
 && cd ffmpeg \
 && PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure \
  --prefix="$HOME/ffmpeg_build" \
  --extra-cflags="-I$HOME/ffmpeg_build/include" \
  --extra-ldflags="-L$HOME/ffmpeg_build/lib" \
  --bindir="/usr/local/bin" \
  --enable-gpl \
  --enable-libass \
  --enable-libfdk-aac \
  --enable-libfreetype \
  --enable-libmp3lame \
  --enable-libopus \
  --enable-libtheora \
  --enable-libvorbis \
  --enable-libvpx \
  --enable-libx264 \
  --enable-nonfree \
 && PATH="$HOME/bin:$PATH" make \
 && make install \
 && make distclean \
 && hash -r

RUN apt-get install ffmpegthumbnailer ffmpeg2theora -y

# various image and pdf processing software
RUN apt-get install pdftk -y
RUN apt-get install poppler-utils poppler-data -y
RUN apt-get install graphicsmagick -y

WORKDIR $HOME
# Install wkhtmltopdf version 0.12
RUN apt-get install -y xfonts-base xfonts-75dpi fontconfig
RUN wget http://ftp.de.debian.org/debian/pool/main/libj/libjpeg-turbo/libjpeg62-turbo_1.5.2-2+deb10u1_amd64.deb
RUN dpkg -i libjpeg62-turbo_1.5.2-2+deb10u1_amd64.deb
# the package asked for fontconfig, maybe the other (xfonts...) packages aren't necessary anymore
RUN wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.buster_amd64.deb
RUN dpkg -i wkhtmltox_0.12.5-1.buster_amd64.deb

# Install gs version 9.15
USER app
WORKDIR $HOME
# ghostscript makes older realeases seemingly unavailable
RUN wget https://github.com/ArtifexSoftware/ghostpdl-downloads/releases/download/gs10020/ghostscript-10.02.0.tar.gz
RUN tar xzf ghostscript-10.02.0.tar.gz
WORKDIR $HOME/ghostscript-10.02.0
RUN sed -i 's/ZLIBDIR=src/ZLIBDIR=$includedir/' configure.ac configure
RUN rm -rf zlib && ./configure --prefix=/usr
RUN make

USER root
RUN make install

# and docsplit finally
RUN gem install docsplit
